function deepCopy(object) {
    return JSON.parse(JSON.stringify(object));
}

Array.prototype.remove = function (element) {
    for (var i = this.length - 1; i >= 0; i--) {
        if (this[i] === element) {
            this.splice(i, 1);
            return true;
        }
    }
    return false;
};

Array.prototype.contains = function (element) {
    for (var i = this.length - 1; i >= 0; i--) {
        if (this[i] === element) {
            return true;
        }
        return false;
    }
};

Array.prototype.toggle = function (element) {
    if (!this.remove(element))
        this.push(element);
}
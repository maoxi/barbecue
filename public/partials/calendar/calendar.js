barbecueApp.controller('CalendarCtrl', function ($scope, LoginData) {
    //$scope.day =moment();    //in view: selected="day"  pass the day value to directive
});

barbecueApp.directive('calendar', ['$http', 'LoginData', function ($http, LoginData) {
    return {
        restrict: "E",
        templateUrl: "template/calendar.html",
        scope: {
            selected: "="   /*@' means the variable will be copied (cloned) into the directive.*/  // selected = selected(in view)
        },
        controller: function ($scope, LoginData) {   //Linker is tied to each instance

            $scope.selected = _removeTime($scope.selected || moment());
            $scope.month = $scope.selected.clone(); //.add(1, 'M');
            $scope.bookings = [];
            $scope.bookingMap = [];

            $scope.refreshMapFromBookings = function () {
                $scope.bookingMap = [];
                $scope.bookings.forEach(function (booking) {
                    booking.days.forEach(function (day) {
                        if ($scope.bookingMap[day]) {
                            $scope.bookingMap[day].push(booking.person);
                        } else {
                            $scope.bookingMap[day] = [booking.person];
                        }
                    });
                });
            };

            var start = $scope.selected.clone(); //.add(1, 'M');
            start.date(1); //get the first day of this month
            _removeTime(start.day(0)); // get the Sunday of this week
            _buildMonth($scope, start, $scope.month);

            $scope.refreshCalendar = function () {
                $http.get('/calendar').success(function (data) {
                    $scope.bookings = data;
                    $scope.refreshMapFromBookings();
                    _buildMonth($scope, start, $scope.month);
                });
            };

            $scope.chosenDay = function (day) {
                var bookingsThatDay = $scope.bookingMap[day.date.format("MM/DD/YYYY")];
                if (bookingsThatDay) {
                    return bookingsThatDay.join(', ');
                } else {
                    return '';
                }
            };

            $scope.select = function (day) {

                $http.post('/calendar/toggle', {
                    person: LoginData.getUserName(),
                    day: day.date.format("MM/DD/YYYY")
                }).success(function (res) {
                    $scope.refreshCalendar();
                });
            };

            $scope.next = function () {
                var next = $scope.month.clone();
                _removeTime(next.month(next.month() + 1).date(1));  // set "next.month" to the first day of the next month
                $scope.month.month($scope.month.month() + 1);    //set the "$scope.month" to the next month
                _buildMonth($scope, next, $scope.month);
                start = next;
            };

            $scope.previous = function () {
                var previous = $scope.month.clone();
                _removeTime(previous.month(previous.month() - 1).date(1));
                $scope.month.month($scope.month.month() - 1);
                _buildMonth($scope, previous, $scope.month);
                start = previous;
            };

            $scope.refreshCalendar();
        }
    };


    function _removeTime(date) {
        return date.day(0).hour(0).minute(0).second(0).millisecond(0);
    }

    // need the month and the start day
    function _buildMonth($scope, start, month) {
        $scope.weeks = [];
        var done = false, date = start.clone(), monthIndex = date.month(), count = 0;

        while (!done) {
            $scope.weeks.push({days: _buildWeek(date.clone(), month)});
            date.add(1, "w");  //add one week
            done = count++ > 2 && monthIndex !== date.month(); //till date.month() change to the next month
            monthIndex = date.month();  //because of the first week
        }
    }

    function _buildWeek(date, month) {
        var days = [];
        for (var i = 0; i < 7; i++) {
            days.push({
                name: date.format("dd").substring(0, 1),
                number: date.date(),
                isCurrentMonth: date.month() === month.month(),
                isToday: date.isSame(new Date(), "day"),
                date: date
            });
            date = date.clone();
            date.add(1, "d");
        }
        return days;
    }
}]);



'use strict';

var barbecueApp = angular.module('barbecueApp', [
    'ngRoute', 'bbqPicServices'
]);

barbecueApp.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider
            .when('/foodList', {
                templateUrl: 'partials/foodList/foodList.html',
                controller: 'FoodListCtrl'
            }).
            /* when('/phones/:phoneId',{
             templateUrl:'partials/detail.html',
             controller: 'DetailCtrl'
             }).*/
            when('/calendar', {
                templateUrl: 'partials/calendar/calendar.html',
                controller: 'CalendarCtrl'
            }).
            when('/bbqPics', {
                templateUrl: 'partials/bbqPics/bbqPics.html',
                controller: 'BbqPicsCtrl'
            }).
            when('/bbqPics/:bbqPicId', {
                templateUrl: 'partials/bbqPics/bbqPicsDetail.html',
                controller: 'BbqPicsDetailCtrl'
            }).
            otherwise({
                redirectTo: '/foodList'
            });
    }]);

//for nav bars
barbecueApp.controller('NavCtrl', function ($scope, $location, LoginData) {
    $scope.pageVisible = false;
    $scope.userName = '';
    $scope.socket = io();
    /*$scope.$watch('userName',function(newValue,oldValue){
     if(newValue!==oldValue){
     LoginData.setUserName($scope.username);
     }
     });*/

    $scope.showWebsite = function (event) {
        if (event.which === 13) {
            if ($scope.userName) {
                $scope.pageVisible = true;
                LoginData.setUserName($scope.userName);
                $scope.socket.emit('add user', LoginData.getUserName());
            } else {
                console.log("log without name");
            }
        }
    };

    $scope.isActive = function (viewLocation) {
        return viewLocation === $location.path();
    };
});


barbecueApp.factory('LoginData', function () {
    var userName = '';
    return {
        getUserName: function () {
            return userName;
        },
        setUserName: function (newName) {
            userName = newName;
        }
    }
});
